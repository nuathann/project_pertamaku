package com.renseki.app.projectpertamaku

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.google.gson.Gson
import com.renseki.app.projectpertamaku.webservices.DummyService
import com.renseki.app.projectpertamaku.webservices.Post
import kotlinx.android.synthetic.main.main_activity_four.*
import kotlinx.android.synthetic.main.main_activity_three.*
import kotlinx.android.synthetic.main.main_activity_two.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        tryWebServices()
    }

    private fun tryWebServices() {
        val gson = Gson()

        val retrofit = Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
        val service = retrofit.create(DummyService::class.java)

        val caller = service.getPosts()
        caller.enqueue(object: Callback<List<Post>> {
            override fun onFailure(call: Call<List<Post>>, t: Throwable) {
                // todo
            }

            override fun onResponse(call: Call<List<Post>>, response: Response<List<Post>>) {
                if (response.isSuccessful) {
                    val postPertama = response.body()?.first()
                    if (postPertama != null) {
                        Toast
                                .makeText(
                                        this@MainActivity,
                                        postPertama.title,
                                        Toast.LENGTH_LONG
                                )
                                .show()
                    }
                }
            }
        })
    }

    private fun initFive() {
       val intent = Intent(this, SecondActivity::class.java)
        startActivity(intent)
    }

    private fun initFour() {
        setContentView(R.layout.main_activity_four)

        add.setOnClickListener {
            val strFirst = ed_first.text.toString()
            val strSecond = ed_second.text.toString()

            val intFirst = strFirst.toInt()
            val intSecond = strSecond.toInt()

            val res = intFirst + intSecond
            tv_result.text = "Hasilnya adalah $res"
        }
    }

    private fun String.debugToast(context: Context) {
        if (BuildConfig.DEBUG) {
            Toast
                    .makeText(context, this, Toast.LENGTH_LONG)
                    .show()
        }
    }

    private fun initThree() {
        setContentView(R.layout.main_activity_three)

        click_me.setOnClickListener {
            getString(R.string.sesuatu).debugToast(this)
        }
    }

    private fun initTwo() {
        setContentView(R.layout.main_activity_two)

        val strFirst = first.text.toString()
        val strSecond = second.text.toString()

        val intFirst = strFirst.toInt()
        val intSecond = strSecond.toInt()

        val res = intFirst + intSecond
        result.text = "Hasilnya adalah $res"
    }
}
