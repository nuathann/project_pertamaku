package com.renseki.app.projectpertamaku.webservices

import retrofit2.Call
import retrofit2.http.GET

interface DummyService {
    @GET("posts")
    fun getPosts(): Call<List<Post>>
}